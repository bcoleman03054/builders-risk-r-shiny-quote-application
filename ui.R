####---- Bankers Insurance - Commercial Lines - Monthly Reporting Application ----#####

#####---- User Interface Script ----#####


ui <- dashboardPage(
  dashboardHeader(title = "Bankers Insurance - Builders Risk Monthly Reporting",
                  titleWidth = 500), 
  dashboardSidebar(
    title = 'Please Select a Page',
    width = 350,
    sidebarMenu(
      menuItem("Builders Risk Quote Page", tabName = "MonthlyQuote", icon = icon("file")),
      menuItem("Monthly Reporting Information", tabName = "ReportingInformation", icon = icon("address-card")),
      menuItem("Reporting Form", tabName = "ReportingForm", icon = icon("table")),
      menuItem('Search Reporting Forms', tabName = 'SearchReportingForms', icon = icon('search'))
      
      
      
    ) #sidebar menu##
    
    
    
  ), ##dashboard sidebar###
  
  dashboardBody(
    tags$head(tags$style(HTML('
                              .skin-blue .main-header .logo {
                              background-color: #000762;
                              }
                              .skin-blue .main-header .logo:hover {
                              background-color: #000762;
                              }
                              /* navbar (rest of the header) */
                              .skin-blue .main-header .navbar {
                              background-color: #000762;
                              }        
                              
                              /* main sidebar */
                              .skin-blue .main-sidebar {
                              background-color: #000762;
                              }
                              
                              /* active selected tab in the sidebarmenu */
                              .skin-blue .main-sidebar .sidebar .sidebar-menu .active a{
                              background-color: #1EA904;
                              }
                              
                              /* other links in the sidebarmenu */
                              .skin-blue .main-sidebar .sidebar .sidebar-menu a{
                              background-color: #000762;
                              color: #FFFFFF;
                              }
                              
                              /* other links in the sidebarmenu when hovered */
                              .skin-blue .main-sidebar .sidebar .sidebar-menu a:hover{
                              background-color: #3F403F;
                              }
                              /* toggle button when hovered  */                    
                              .skin-blue .main-header .navbar .sidebar-toggle:hover{
                              background-color: #3F403F;
                              }
                              /* Modal Formatting  */ 
                              .modal-content{background-color: #009310; font-size: 22px}
                              .modal-dialog {text-align: center; vertical-align: center;}
                              .modal-header {background-color: #009310; border-top-left-radius: 6px; border-top-right-radius: 6px}
                              .modal { text-align: center;}
                              .close { font-size: 20px}
                              
                              '))), ## Webpage Aesthetics 
    tags$link(rel="stylesheet", type="text/css",href="style.css"),
    tags$script(type="text/javascript", src = "md5.js"),
    tags$script(type="text/javascript", src = "passwdInputBinding.js"),
    tags$script(src="format_numbers.js"),
    
    
    
    useShinyjs(),
    actionButton("showSidebar", "Show sidebar", style="color: #fff; background-color: #1700FF; border-color: #37F0FF"),
    actionButton("hideSidebar", "Hide sidebar", style="color: #fff; background-color: #1700FF; border-color: #37F0FF"),
    bookmarkButton('Bookmark or Save', style="color: #000000; background-color: #FCFF1C; border-color: #000000"),
    tabItems(

      
      tabItem(tabName = "MonthlyQuote",
              
              ##Boxes need to be put in a row (or column)##
              
              fluidRow(
                box(
                  title = "Applicant Information", background = "navy", collapsible = FALSE, width = 5,
                  selectInput('Policy.Type', 'Policy Type', c('', 'Single Shot', 'Monthly Reporting'), selected = ''),
                  dateInput('Date.of.Quote', 'Date of Quote', min = Sys.Date(), format = 'mm-dd-yyyy'),
                  dateInput("Quote.Effective.Date", "Desired Effective Date", min = Sys.Date()+1 , max = Sys.Date() + 730, format = 'mm-dd-yyyy'),
                  selectInput('Reporting.Agent', 'Agent Name', sort(Agent.Master$Reporting.Agent, decreasing = FALSE), selected = devAgentName),
                  htmlOutput('MonAID'),
                  textInput("Reporting.Producer", "Producer Name", ""),
                  textInput("Reporting.Applicant.Name", "Applicant Name", devApplicantName),
                  textInput("Reporting.Applicant.Phone", "Applicant Phone Number", ""),
                  textInput("Reporting.Loss.Payee", "Loss Payee Name", ""),
                  selectInput("Reporting.Applicant.Interest", "Interest of Applicant", c("","Owner", "Contractor", "Other"), selected = ""),
                  selectInput("Quote.Business.Type", "Type of Business", c("", "Sole Proprietorship", "Partnership", "LLC", "Corporation", 'Other'))
                  
                  
                  
                ), ##(box applicant information)##
                
                fluidRow(
                  box(
                    title = 'Location Information', background = "navy", collapsible = TRUE, width = 5,
                    textInput("Quote.Street.Address", "Street Address", devStreetAddress),
                    numericInput("Quote.Zip.Code", "Zip Code", value = devZip),
                    htmlOutput('MonQuoteCity'),
                    htmlOutput('MonQuoteCounty'),
                    htmlOutput('MonQuoteState'),
                    actionButton("showMap", "Show Map of Address", icon = icon('map'), style="color: #fff; background-color: #1700FF; border-color: #37F0FF", width = '100%')
                    
                    
                    
                    
                    
                  ), ##box location information##
                  
                  
                  box(title = "Risk Information", background = "navy", collapsible = TRUE, collapsed = TRUE, width = 5,
                      selectInput("Quote.Construction.Type", "Construction Type", c("Frame", "Joisted Masonry", "Non Combustible", "Masonry Non Combustible", "Modified Fire Resistive", "Fire Resistive", ""), selected = 'Frame'),
                      selectInput("Quote.Protection.Class", "Protection Class", c('', '1 to 8', '9 and 10'), '1 to 8'), 
                      htmlOutput('Duration'),
                      htmlOutput('AvgOrTotVal'),
                      htmlOutput('InProgressOrProjType')
                      
                      
                      
                      
                      
                  ), ###risk information box##
                  
                  
                  box(title = 'Coverage & Deductible Information', background = 'navy', collapsible = TRUE, collapsed = TRUE, width = 5,
                      selectInput('Earthquake.Cov.Indicator', 'Earthqake Coverage?', c('', 'Yes', 'No'), selected = devEQ), 
                      selectInput('Wind.Cov.Indicator', 'Exclude Wind Coverage?', c('', 'Yes', 'No'), selected = devWind),
                      selectInput('Quote.Wind.Deductible', 'Wind Deductible', choices = c(''), selected = devWindDED),
                      selectInput('Quote.AOP.Deductible', 'AOP Deductible', as.character(AOP.Deductible.Rate.Master$Deductible), selected = devAOPDED),
                      actionButton('Submit.Quote', "Submit Your Quote!", width = '100%', style="color: #fff; background-color: #009310; border-color: #19EA2F")
                      
                      
                      
                  ),  ## Coverage and Deductible Information Box ##
                  
                  
                  box(title = 'Rating Information', background = 'navy', collapsible = TRUE, collapsed = TRUE, width = 5,
                      textOutput('MonQuoteTerritoryRate'),
                      textOutput('MonQuoteDurationRate'), 
                      textOutput('MonQuoteEQLoad'),
                      textOutput('BasicRate'),
                      textOutput('AOPDedFactor'),
                      textOutput('QuoteWindDeductibleFactor'),
                      textOutput('QuoteWindExclusionFactor'),
                      textOutput('QuoteFinalRate'),
                      textOutput('QuoteLimitPer100'),
                      textOutput('QuoteFinalPremium')
                  ), 
                  
                  fluidRow(
                    
                    textOutput('QuotePriceQuickView'),
                    br(),
                    textOutput('QuoteDisclaimer')
                    
                    
                  )
                  
                ) ##fluid row 2 - Quote##
                
                
              ) ##fluid row 1 - Quote##
              
              
              
      ),##TAB 2 - Monthly Quote##
      
      
      
      
      tabItem(tabName="ReportingInformation",
              fluidRow(
                box(title = "Monthly Reporting Information", background = "navy",width = 5
                    
                    
                    
                    
                ) ##Agent Information Box - Reporting Information
                
                
              ) ##fluid row 1 - Reporting Information
              
              
              
              
              
      ), ##TAB 3 - Reporting Information ###
      
      tabItem(tabName = "ReportingForm",
              fluidRow(
                box(title = 'Reporting Agent Information', background = 'navy', width = 5,
                    htmlOutput('RFAgentName', inline = TRUE),
                    dateInput('Reporting.Form.Date', 'Reporting Date', min = Sys.Date(), format = 'mm-dd-yyyy')
                    
                    
                    
                ), ## Reporting Agent Information Box - Reporting Form 
                
                box(title = 'Spreadsheet Upload', background = 'navy', width = 5, 
                    fileInput('MonReportSpeadUpload', label = 'Click Browse to Upload Your Report', multiple = FALSE, accept = c('.text/csv', 'text/comma-separated-values', 'text/tab-separated-values', 'text/plain', '.csv', '.txt', '.xlsx', '.xls')),
                    actionButton('Save.Spreadsheet.Upload', 'Click Here to Save Your Upload!', width = '100%', style="color: #fff; background-color: #009310; border-color: #19EA2F")
                    
                    
                    
                    
                ), ## Box - Monthly Reporting Spreadsheet 
                
                textOutput('UploadPremium'),
                
                column(DTOutput('Reporting.Spreadsheet'), width = 10)
                
              ) ##Fluid Row - Reporting Form
              
              
      ),  ###TAB 4 - Reporting Form
      
      tabItem(tabName = 'SearchReportingForms',
              fluidRow(
                box(title = 'Search Reporting Forms', background = 'navy', width = 10,
                    htmlOutput('SearchAgentName'),
                    selectInput('Reporting.Month.Search', 'Month to Search', choices = as.character(Month.Master2$`Month Name`)),
                    actionButton('SearchButton', 'Click to Search!', width = '100%', style="color: #fff; background-color: #1700FF; border-color: #37F0FF")
                    
                    
                ), ##Box - Search Reporting Forms
                
               
                
                column(title = 'Reporting Form Search Results', width = 10,
                       DTOutput('Search.Display.Table')
                       
                       
                ) ##Column - Reporting Form Search Results 
                
              ) ##Fluid Row - Search Form
              
      ) ##Tab 5 - Search Reporting Forms
      
      
    ) ##tab item control
    
    
    
    ) ###dashboard body
  
  
  
    )  ###dashboard page###