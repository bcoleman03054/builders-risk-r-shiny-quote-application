Builders Risk - R Shiny Quote Application

This Shiny application is a prototype of the Agent / Consumer Builders Risk Portal.
The application utilized 3 different scripts to create the applicaiton: The UI Script, The Server Script, and the Global Script. 

The UI script -  nested R functions that assemble an HTML user interface for the app 

The Server Script -  a function with instructions on how to build and rebuild the R objects displayed in the UI 

The Global Script - Objects defined in global.R are outside of the server function definition, with one important difference: they are also visible to the code in the ui object.
This is because they are loaded into the global environment of the R session; all R code in a Shiny app is run in the global environment or a child of it.

For a brief overview of how all the scripts work together, check out the link below for a cheatsheet on RShiny Web Apps.
https://shiny.rstudio.com/images/shiny-cheatsheet.pdf

Please refer all questions to commercial product managment. 